import { Test, TestingModule } from '@nestjs/testing';
import { PdfTemplateBuilderService } from './pdf-template-builder.service';

describe('PdfTemplateBuilderService', () => {
  let service: PdfTemplateBuilderService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PdfTemplateBuilderService],
    }).compile();

    service = module.get<PdfTemplateBuilderService>(PdfTemplateBuilderService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
