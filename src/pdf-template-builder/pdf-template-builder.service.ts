import { Injectable, HttpService } from '@nestjs/common';
import { join } from 'path';
import * as fs from 'fs';
import * as ejs from 'ejs';

const TEMPLATES_PATH = join(__dirname, '/../assets/templates/');

@Injectable()
export class PdfTemplateBuilderService {
  constructor(private http: HttpService) { }

  async objToHtmlTemplate(templateId: string, data: any) {
    const templateStr = fs.readFileSync(
      TEMPLATES_PATH + `template_${templateId}.ejs`,
      {
        encoding: 'utf-8',
      },
    );

    const html = ejs.render(templateStr, data);
    return html;
  }
}
