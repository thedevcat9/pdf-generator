import { Injectable } from '@nestjs/common';
import * as puppeteer from 'puppeteer';

@Injectable()
export class PdfOptionsService {
  generatePdfOptions() {
    return {
      format: 'A4',
      landscape: true,
      displayHeaderFooter: true,
      footerTemplate: `
      <div style="color: lightgray; font-size: 10px; text-align: center; width: 100%;">
        <span class="pageNumber"></span> / <span class="totalPages"></span>
      </div>
    `,
      margin: {
        bottom: 50,
        left: 25,
        right: 35,
        top: 30,
      },
      printBackground: true,
    } as puppeteer.PDFOptions;
  }
}
