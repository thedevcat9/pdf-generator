import { Test, TestingModule } from '@nestjs/testing';
import { PdfOptionsService } from './pdf-options.service';

describe('PdfOptionsService', () => {
  let service: PdfOptionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PdfOptionsService],
    }).compile();

    service = module.get<PdfOptionsService>(PdfOptionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
