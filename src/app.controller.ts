import { Controller, Res, Param, Body, Post } from '@nestjs/common';
import { PdfTemplateBuilderService } from './pdf-template-builder/pdf-template-builder.service';
import { PdfOptionsService } from './pdf-options/pdf-options.service';
import * as puppeteer from 'puppeteer';
import { Response } from 'express';

@Controller('/template/')
export class AppController {
  constructor(
    private readonly pdfTemplateBuilder: PdfTemplateBuilderService,
    private readonly pdfOptionsBuilder: PdfOptionsService,
  ) { }

  @Post(':id')
  async generatePdfFromReqObjAndTemplate(@Param() params, @Body() reqBody: any, @Res() res: Response) {
    const templateId = params.id;
    const pdfData = reqBody;

    const html = await this.pdfTemplateBuilder.objToHtmlTemplate(
      templateId,
      pdfData,
    );

    const browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox', '--disable-setuid-sandbox'],
    });

    const page = await browser.newPage();
    const pdfOptions = this.pdfOptionsBuilder.generatePdfOptions();

    await page.setContent(html, {
      waitUntil: ['domcontentloaded', 'load'],
    });

    const result = await page.pdf(pdfOptions);

    await browser.close();

    res.send(result);
  }
}
