import { Module, HttpModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PdfTemplateBuilderService } from './pdf-template-builder/pdf-template-builder.service';
import { PdfOptionsService } from './pdf-options/pdf-options.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    HttpModule,
  ],
  controllers: [AppController],
  providers: [AppService, PdfTemplateBuilderService, PdfOptionsService],
})
export class AppModule {}
