import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as compression from 'compression';
import { AppModule } from './app.module';

async function bootstrap() {
  const port = process.env.PORT;
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.use(compression());
  app.enableCors();
  await app.listen(port);
  Logger.log(`Listening on PORT ${port}`, 'NestExpressApplication');
}
bootstrap();
